# OnlyTournament

## Project setup
```
npm install
npm run serve
```
[http://localhost:8080]

### Compiles and minifies for production
```
npm run build
```
### Lints and fixes files
```
npm run lint
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Fix for the build error from nodejs 17.0.1
```
 "scripts": {
    "serve": "SET NODE_OPTIONS=--openssl-legacy-provider && vue-cli-service serve",
    "build": "SET NODE_OPTIONS=--openssl-legacy-provider && vue-cli-service build",
    "lint": "SET NODE_OPTIONS=--openssl-legacy-provider && vue-cli-service lint"
  },
```
### Icons
```
https://www.flaticon.com/free-icon/winner_1021220?term=trophy&related_id=1021118&origin=search
https://icones8.fr/
```
### License
```
Copyright 2021 Elouan DUVAL
```