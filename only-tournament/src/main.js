import Vue from 'vue'
import App from './App/components/app/App.vue'
import router from './router'
//import Vuetify from 'vuetify'
//import 'vuetify/dist/vuetify.min.css'
//import axios from 'axios'
//import './App/components/plugins/apexcharts'

//Vue.prototype.$http = axios;
//Vue.config.productionTip = false
//Vue.use(Vuetify)

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')